package core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class GitRepository {
    private String repoPath;

    public GitRepository(String path)
    {
        this.repoPath = path;
    }

    public String getHeadRef() {

        String headPath = repoPath+"/HEAD";
        try (BufferedReader bf = new BufferedReader(new FileReader(headPath))) {
            String line = bf.readLine();
            return line.substring(5);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";

    }

    public String getRefHash(String s) {
        String hashPath = repoPath+"/"+s;
        try (BufferedReader bf = new BufferedReader(new FileReader(hashPath))) {
            String line = bf.readLine();
            return line;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
